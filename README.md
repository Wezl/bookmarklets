# bookmarklets

My bookmarklet collection as I learn javascript.

### others/

Bookmarklets I found but didn't make

### finished/

Minimized and URL encoded ready to use bookmarks.

## I just want to use them!

[ViMode](notworking)

[TextofPageOnly](alsonotworking)

[make more](https://bookmarklets.org/maker/)
